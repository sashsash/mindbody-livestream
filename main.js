// ==UserScript==
// @name         MindBody-Stream-Email
// @namespace    http://tampermonkey.net/
// @version      0.11
// @description  try to take over the world!
// @author       You
// @match        https://www.tampermonkey.net/index.php?version=4.9&ext=dhdg&updated=true
// @grant        none
// @include      https://clients.mindbodyonline.com/classic/admclslist*
// @downloadURL	https://bitbucket.org/sashsash/mindbody-livestream/raw/master/main.js
// @updateURL	https://bitbucket.org/sashsash/mindbody-livestream/raw/master/main.js


// ==/UserScript==


(function () {
    'use strict';

    // none of these are used yet. CSS FFS
    var cssStyle = `
    <style>
    #ytOptions{
    
    
    
    }
    
    #streamlink{
    
    
    }
    
    .instructions{
    list-style-type: circle;
    }
    
    </style>
    
    
    `
    //HTML that will be injected
    var htmlForm = `<div id="ytOptions"><h3>Youtube Streaming</h3>
    
     
        <h4>Step 1:</h4>
<br>    <p><a id="launchClass" href="https://www.youtube.com/livestreaming/webcam" target="_blank">For reception - Launch New Live Stream </a></p>
<p>Change the thumnail</p>
    <ul class="instructions">
    <li> - On YouTube...</li>
    <li> - Click SHARE and copy the shareable link</li>
    
    <li> - Return to Mind Body and paste the link below</li>
    <br>
    <h4>Step 2:</h4>
    <form id="mailForm" action="" method="GET">
      <label for="streamlink">Paste the Youtube Webcam Share Link</label>
      <input type="text" size="36" class="form-control" id="streamlink" >
    <input id="emailStudents" type="button" value="Email Students" ></input>
    </form>
    <p> - Return to Youtube</p>
    <p><a href="https://www.youtube.com/livestreaming/manage" target="_blank">In the studio find your stream - Manage Live Streams </a></p>
    <p>Click GO LIVE and teach!</p>
    </div>
    <br>
    <p>-----------------</p>
      
    <div id=emailContent style="display:none">
    <b>Your class is about to begin</b>
    <div id="classDetails" >
    </div>
    <br>
    <p>You can access the live stream of your class here <a id="classlink" href=""></a></p>
    <br>
    <p>We hope you enjoy the class and we're looking forward to seeing you soon!<p>
    
  
    </div>
    </div>`



    var mylist = document.createElement("UL");
    var mailtoString = 'mailto:contact@trikayoga.co.uk'
    var subject = '&subject=Trika%20Yoga%20Video%20Link%20for%20your%20class'
    var mailBody = ""
    // var mailBody = "&body=Your%20Video%20link%20for%20today's%20class%20is%20"
    var usersString = '?bcc='

    var classTitle = $('.headText b')[0].innerHTML.replace('Class Sign In', '')

    // function to loop through the users table, get user IDs and sign them up. Currently disabled
    function checkinEveryone() {



        $('.center-ch').each(function () {

            let id = this.id
            if (id != '') {
                console.log(id)
                updateSignedIn(id, false);
            }
        })

    }


    // build the mailto: link and launch it
    function buildMailTo() {

        //  var mailto_link = mailtoString + usersString + subject + mailBody + pasteMessage
        var mailto_link = mailtoString + usersString + subject
        window = window.open(mailto_link, "emailWindow")
    }

    //populate the injected HTML with required values
    function populateHTMLEmail() {

        //Populate the email with the class details

        $('#classDetails').html(classTitle)

        //Grab the hyperlink from the pasted box
        var ytLink = $("#streamlink").val()
        $('#classlink').html(ytLink)
        $('#classlink').attr("href", ytLink)



    }


    //function to copy HTML contents as rich content
    function copyToClip(str) {
        function listener(e) {
            e.clipboardData.setData("text/html", str);
            e.clipboardData.setData("text/plain", str);
            e.preventDefault();
        }
        document.addEventListener("copy", listener);
        document.execCommand("copy");
        document.removeEventListener("copy", listener);
    };


    //inject the HTML we require
    $("#main-content").append(cssStyle);
    $("#main-content").append(htmlForm);

    //attempt to change list css - doesn't work
    $(".instructions").css("list-style-type", "circle");


    $('#launchClass').click(function () {


        var title = $('<textarea />').html(classTitle).text();
        title = title.replace(/\t\t/g, ' ')
        title = title.replace('  ', ' ')
        title = title.replace('  ', ' ')
        title = title.replace('  ', ' ')
        title = title.replace('  ', ' ')
        title = title.replace('  ', ' ')
        title = title.replace('  ', ' ')
        title = title.replace('  ', ' ')
        title = title.replace('  ', ' ')
        title = title.replace('  ', ' ')
        copyToClip(title.replace(/<[^>]*>?/gm, ''))



    })

    $("#emailStudents").click(function () {

        //check in all users
        checkinEveryone()

        // build the email
        populateHTMLEmail()

        //copy rich email to clipboard
        copyToClip(document.getElementById('emailContent').innerHTML)

        //build final email and pop the email window
        buildMailTo()
    });


    //get user email addresses as soon as the page is loaded
    $('a[href^="mailto:"]').each(function () {

        //append users list
        usersString = usersString + this.pathname + ','

        // also write email addresses to HTML body for easy copy
        var node = document.createElement("LI");
        var textnode = document.createTextNode(this.pathname);
        node.appendChild(textnode);
        mylist.appendChild(node);



    })

    //append user emails to the document
    document.getElementById("wrapper-bottompad").appendChild(mylist);




})();